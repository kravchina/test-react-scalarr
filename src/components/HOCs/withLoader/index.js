import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import Loader from '../../Loader';

function withLoader(WrappedComponent, ...actions) {
  class LoaderWrapper extends PureComponent {
    render() {
      const { loadingItems } = this.props;
      let isLoading = false;

      actions.some(current => {
        const isExist = loadingItems.find(i => i === current.split(' ')[0]);
        if (isExist) {
          isLoading = true;
          return true;
        }
      });

      return (
        <div>
          {
            isLoading
              ?
            <Loader/>
              :
            <WrappedComponent {...this.props}/>
          }
        </div>
      )
    }
  }

  const mapStateToProps = state => ({
    loadingItems: state.loadingReducer.loadingItems,
  });

  return connect(mapStateToProps, null)(LoaderWrapper);
}

export default withLoader;
