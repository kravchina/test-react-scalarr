import React, { Component }  from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { loadSecondBlock } from '../../../store/actions/blocksActions';
import * as types from '../../../constants/actionTypes';
import withLoader from '../../HOCs/withLoader/index';
import BlockItem from '../BlockItem/index';

class SecondBlock extends Component {

  componentDidMount() {
    const { secondBlockData, loadData } = this.props;
    if (!secondBlockData.length) {
      loadData();
    }
  }

  render() {
    return (
      <div className='second-block'>
        <h2>second block</h2>
        <BlockItem data={this.props.secondBlockData}/>
      </div>
    )
  }
}

SecondBlock.propTypes = {
  secondBlockData: PropTypes.array.isRequired,
};

const mapStateToProps = state => ({
  secondBlockData: state.blocksReducer.secondBlock,
});

const mapDispatchToProps = dispatch => ({
  loadData: () => dispatch(loadSecondBlock()),
});

export default connect(mapStateToProps, mapDispatchToProps)(withLoader(SecondBlock, types.SECOND_BLOCK_START_LOAD));
