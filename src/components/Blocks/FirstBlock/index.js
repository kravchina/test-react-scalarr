import React, { Component }  from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { loadFirstBlock } from '../../../store/actions/blocksActions';
import * as types from '../../../constants/actionTypes';
import withLoader from '../../HOCs/withLoader/index';
import BlockItem from '../BlockItem/index';

class FirstBlock extends Component {

  componentDidMount() {
    const { firstBlockData, loadData } = this.props;
    if (!firstBlockData.length) {
      loadData();
    }
  }

  render() {
    return (
      <div className='first-block'>
        <h2>first block</h2>
        <BlockItem data={this.props.firstBlockData}/>
      </div>
    )
  }
}

FirstBlock.propTypes = {
  firstBlockData: PropTypes.array.isRequired,
};

const mapStateToProps = state => ({
  firstBlockData: state.blocksReducer.firstBlock,
});

const mapDispatchToProps = dispatch => ({
  loadData: () => dispatch(loadFirstBlock()),
});

export default connect(mapStateToProps, mapDispatchToProps)(withLoader(FirstBlock, types.FIRST_BLOCK_START_LOAD));
