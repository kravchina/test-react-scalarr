import React from 'react';
import Image from "../../Image";

const ThirdBlock = () => (
  <div className='third-block'>
    <h2>third block</h2>
    <Image src='https://images.unsplash.com/photo-1478562853135-c3c9e3ef7905'/>
  </div>
);

export default ThirdBlock;
