import React   from 'react';
import './index.css';

const BlockItem = ({ data }) => (
  <div className='block-item'>
    <ul className='items-list'>
      {
        data.map(user => (
          <li key={user.id}>
            <span>
              <img src={user.avatar} width={30}/>
            </span>
            <span>
              {`${user.first_name} ${user.last_name}`}
            </span>
          </li>
        ))
      }
    </ul>
  </div>
);

export default BlockItem;
