import React, { Component } from 'react';
import PropTypes from 'prop-types';
import loaderImg from '../../assets/loader.gif';

class Image extends Component {

  constructor(props) {
    super(props);
    this.state = { loading: false };
    this.image = React.createRef();
  }

  componentDidMount() {
    const img = this.image.current;
    if (img && img.complete) {
      this.handleImageLoaded();
    }
  }

  handleImageLoaded = () => {
    if (!this.state.loading) {
      this.setState({ loading: true });
    }
  };

  render() {
    return (
      <div className="image-container">
        {
          !this.state.loading &&
            <img src={loaderImg} />
        }
        <img
          src={this.props.src}
          ref={this.image}
          onLoad={this.handleImageLoaded}
          width={1024}
          hidden={!this.state.loading}
        />
      </div>
    )
  }
}

Image.propTypes = {
  src: PropTypes.string.isRequired,
};

export default Image;