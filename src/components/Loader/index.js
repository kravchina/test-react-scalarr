import React from 'react';
import loaderImg from '../../assets/loader.gif';

export default () => (
  <div className='loader'>
    <img src={loaderImg}/>
  </div>
)