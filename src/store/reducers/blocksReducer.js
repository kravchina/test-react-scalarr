import * as types from '../../constants/actionTypes';

const initialState = {
  firstBlock: [],
  secondBlock: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.FIRST_BLOCK_SUCCESS_LOAD:
      return {
        ...state,
        firstBlock: action.data.data,
      };

    case types.SECOND_BLOCK_SUCCESS_LOAD:
      return {
        ...state,
        secondBlock: action.data.data,
      };

    default:
      return state;
  }
};