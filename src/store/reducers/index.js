import { combineReducers } from 'redux';
import blocksReducer from './blocksReducer';
import loadingReducer from './loadingReducer'

export default combineReducers({
  blocksReducer,
  loadingReducer,
});