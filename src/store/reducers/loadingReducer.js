const initialState = {
  loadingItems: [],
};

const START_LOAD_REGEXP = /START_LOAD/;
const STOP_LOAD_REGEXP = /SUCCESS_LOAD|FAILED_LOAD/;

export default (state = initialState, action) => {
  if (START_LOAD_REGEXP.test(action.type)) {
    const newItem = action.type.split(' ')[0];
    return {
      ...state,
      loadingItems: [
        ...state.loadingItems,
        newItem,
      ],
    };
  } else if (STOP_LOAD_REGEXP.test(action.type)) {
    const itemToRemove = action.type.split(' ')[0];
    const filteredItems = state.loadingItems.filter(item => item !== itemToRemove);
    return {
      ...state,
      loadingItems: filteredItems,
    };
  } else {
    return state;
  }
};