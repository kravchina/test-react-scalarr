import { takeEvery, put, call } from 'redux-saga/effects';
import * as blockActions from '../actions/blocksActions';
import api from '../../constants/apiEndpoints';
import * as types from '../../constants/actionTypes';
import { retryApiCall } from '../axios';


function* loadSecondBlock() {
  try {
    const secondBlockData = yield call(retryApiCall, {
      url: api.secondBlock,
      method: 'get'
    });

    yield put(blockActions.secondBlockSuccess(secondBlockData));
  } catch (error) {
    yield put(blockActions.secondBlockFailed(error));
  }
}


export default function* () {
  yield takeEvery(types.SECOND_BLOCK_START_LOAD, loadSecondBlock);
}