import { takeEvery, put, call } from 'redux-saga/effects';
import * as blockActions from '../actions/blocksActions';
import api from '../../constants/apiEndpoints';
import * as types from '../../constants/actionTypes';
import { retryApiCall } from '../axios';


function* loadFirstBlock() {
  try {
    const firstBlockData = yield call(retryApiCall, {
      url: api.firstBlock,
      method: 'get'
    });

    yield put(blockActions.firstBlockSuccess(firstBlockData));
  } catch (error) {
    yield put(blockActions.firstBlockFailed(error));
  }
}


export default function* () {
  yield takeEvery(types.FIRST_BLOCK_START_LOAD, loadFirstBlock);
}