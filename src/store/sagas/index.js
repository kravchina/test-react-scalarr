import { fork } from 'redux-saga/effects';
import firstBlock from './firstBlockSaga';
import secondBlock from './secondBlockSaga';

export default function* () {
  yield fork(firstBlock);
  yield fork(secondBlock);
}