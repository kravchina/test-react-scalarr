import axios from 'axios';
import { call } from 'redux-saga/effects';
import { delay } from 'redux-saga';

const RETRY_COUNT = process.env.REACT_APP_API_RETRY_COUNT;

axios.defaults.baseURL = process.env.REACT_APP_API_URL;
axios.defaults.timeout = process.env.REACT_APP_API_TIMEOUT;


axios.interceptors.response.use(function (response) {
  return response.data;
}, function (error) {
  console.error('API ERROR', error);
  return Promise.reject({
    msg: 'Request failed',
    error,
  });
});

export function* retryApiCall(params) {
  for (let i = 0; i < RETRY_COUNT; i++) {
    try {
      const apiResponse = yield call(axios, params);
      return apiResponse;
    } catch(err) {
      if(i < RETRY_COUNT - 1) {
        yield call(delay, 2000);
      }
    }
  }
  throw new Error('API request failed');
}