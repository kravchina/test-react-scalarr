import * as types from '../../constants/actionTypes';

export const loadFirstBlock = () => ({
  type: types.FIRST_BLOCK_START_LOAD
});

export const firstBlockSuccess = data => ({
  type: types.FIRST_BLOCK_SUCCESS_LOAD,
  data,
});

export const firstBlockFailed = error => ({
  type: types.FIRST_BLOCK_FAILED_LOAD,
  error,
});

export const loadSecondBlock = () => ({
  type: types.SECOND_BLOCK_START_LOAD
});

export const secondBlockSuccess = data => ({
  type: types.SECOND_BLOCK_SUCCESS_LOAD,
  data,
});

export const secondBlockFailed = error => ({
  type: types.SECOND_BLOCK_FAILED_LOAD,
  error,
});