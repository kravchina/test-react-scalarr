import React, { Component } from 'react';
import './index.css';
import FirstBlock from '../../components/Blocks/FirstBlock';
import SecondBlock from '../../components/Blocks/SecondBlock';
import ThirdBlock from '../../components/Blocks/ThirdBlock';

class HomePage extends Component {

  render() {
    return (
      <div className="page home-page">
        <FirstBlock/>
        <SecondBlock/>
        <ThirdBlock/>
      </div>
    )
  }
}

export default HomePage;